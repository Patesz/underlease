package hu.pte.dhizri;

import java.io.FileInputStream;
import java.util.ArrayList;

import hu.pte.dhizri.data.DataProvider;
import hu.pte.dhizri.data.OwnerDBDAO;
import hu.pte.dhizri.data.PropertyDBDAO;
import hu.pte.dhizri.data.RenterDBDAO;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
	
	private static String FXML_ROOT = "src/hu/pte/dhizri/view/"; 			// access path of the FXML files
	//private static String mainFXML = "MainView.fxml";
	private static String ownerFXML = "OwnerView.fxml";
	private static String renterFXML = "RenterView.fxml";
	private static String propertyFXML = "PropertyView.fxml";
	
	
	private static DataProvider oDAO;
	private static DataProvider pDAO;
	private static DataProvider rDAO;
	
	// Creating DBDAO class type object singletons
	// singleton: only one instance can exists of a particular object
	public static DataProvider getOwnerDBDAO() {	// so when it is called it only creates the object if it is not exists
		if(oDAO == null) {
			oDAO = new OwnerDBDAO();									
		}
		return oDAO;								// else just returns it
	}
	
	public static DataProvider getPropertyDBDAO() {
		if(pDAO == null) {
			pDAO = new PropertyDBDAO();
		}
		return pDAO;
	}
	
	public static DataProvider getRenterDBDAO() {
		if(rDAO == null) {
			rDAO = new RenterDBDAO();
		}
		return rDAO;
	}
	
	// FXML start method to initialize the loader(s), panel(s), scene(s) etc.
	public void start(Stage stage) throws Exception {
		ArrayList<FXMLLoader> fxmlLoaderArray = new ArrayList<FXMLLoader>();
		
		TabPane layout = new TabPane();										// creates a TabPane which can store multiple tabs
		layout.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);			// unable to close tabs
		
		Tab propertyTab = new Tab("Ingatlanok");
		layout.getTabs().add(propertyTab);
		
		Tab ownerTab = new Tab("Tulajdonosok");
		layout.getTabs().add(ownerTab);
		
		Tab renterTab = new Tab("Bérlők");
		layout.getTabs().add(renterTab);
		
		for(int i=0; i<layout.getTabs().size(); i++) {
			fxmlLoaderArray.add(new FXMLLoader());							// dynamically adding FXMLLoaders
		}
		
		AnchorPane propertyTabContent = fxmlLoaderArray.get(0).load(new FileInputStream(FXML_ROOT+propertyFXML));
		propertyTab.setContent(propertyTabContent);
		
		AnchorPane ownerTabContent = fxmlLoaderArray.get(1).load(new FileInputStream(FXML_ROOT+ownerFXML));
		ownerTab.setContent(ownerTabContent);
		
		AnchorPane renterTabContent = fxmlLoaderArray.get(2).load(new FileInputStream(FXML_ROOT+renterFXML));
		renterTab.setContent(renterTabContent);
		
		Scene scene = new Scene(layout); 				// initialize a screen and with the layouts (tabs)
		
		stage.setScene(scene);
		stage.setResizable(false);						// fix scene size
		stage.setTitle("Albérlet CRUD");				// sets the scene title
		stage.show();									// render the panel to screen
		
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}