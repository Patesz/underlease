package hu.pte.dhizri.data;

import java.util.ArrayList;

public interface DataProvider {
	
	public ArrayList<Object> getAll() throws Exception;
	
	public boolean insert(Object obj) throws Exception;
	
	//public boolean isIdUnique(Integer id) throws Exception;
	
	public boolean update(Object obj, Integer id) throws Exception;
	
	public void delete(Object obj) throws Exception;
}
