package hu.pte.dhizri.data;

public class Property {
	private Integer id;						// I use primitive data types where I can, because I don't work with databases
	private Integer own_id;
	private Integer rent_id;
	private Short zip;
	private String town;
	private String address;
	private Short room;
	
	public int getId() {				// getter and setter methods
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public int getOwn_id() {
		return own_id;
	}

	public void setOwn_id(Integer own_id) {
		this.own_id = own_id;
	}

	public Integer getRent_id() {
		return rent_id;
	}

	public void setRent_id(Integer renter_id) {
		this.rent_id = renter_id;
	}

	public Short getZip() {			
		return zip;
	}
	
	public void setZip(Short zipCode) {
		this.zip = zipCode;
	}
	
	public String getTown() {
		return town;
	}
	
	public void setTown(String town) {
		this.town = town;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public int getRoom() {
		return room;
	}
	
	public void setRoom(Short room) {
		this.room = room;
	}
	
	
}
