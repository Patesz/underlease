package hu.pte.dhizri.data;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PropertyDBDAO extends DBConnection implements DataProvider {
	
	public ArrayList<Object> getAll() throws Exception {
		ArrayList<Object> back = new ArrayList<Object>();
		
		initConnection();								//DBConnection's function, it's initialize database 
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT id, own_id, renter_id, zip, town, address, room FROM property ORDER BY id ASC");
		//ResultSetMetaData meta = rs.getMetaData();
		while(rs.next()) {
			Integer id = rs.getInt("id");
			Integer own_id = rs.getInt("own_id");
			Integer renter_id = rs.getInt("renter_id");
			Short zip = rs.getShort("zip");
			String town = rs.getString("town");
			String address = rs.getString("address");
			Short room = rs.getShort("room");
			
			Property row = new Property();
			row.setId(id);
			row.setZip(zip);
			row.setOwn_id(own_id);
			row.setRent_id(renter_id);
			row.setTown(town);
			row.setAddress(address);
			row.setRoom(room);
			
			back.add(row);
		}
		rs.close();
		//conn.close();
		return back;
	}
	
	public ArrayList<Boolean> check(Property newRecord) throws Exception {
		boolean isOwnIdMatch = false;
		boolean isRentIdMatch = false;
		
		ArrayList<Boolean> match = new ArrayList<Boolean>();
		ArrayList<Integer> ownId = new ArrayList<Integer>();
		ArrayList<Integer> rentId = new ArrayList<Integer>();
		
		String sqlOwnQuery = "SELECT id FROM owner";
		String sqlRentQuery = "SELECT id FROM renter";
		
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sqlOwnQuery);
		
		while(rs.next()) {
			ownId.add(rs.getInt("id"));
		}
		
		rs = st.executeQuery(sqlRentQuery);
		
		while(rs.next()) {
			rentId.add(rs.getInt("id"));
		}
		
		Integer foreignOwnId = newRecord.getOwn_id();
		Integer foreignRentId = newRecord.getRent_id();
		
		for(int i=0; i<ownId.size(); i++) {
			if(ownId.get(i) == foreignOwnId) {			// if we found a record with the same id as the input id
				isOwnIdMatch = true;					// that's means there is an owner to own this property
				break;									// if owner id is found then we can break the loop
			}
		}
		
		for(int i=0; i<rentId.size(); i++) {
			if(rentId.get(i) == foreignRentId) {		
				isRentIdMatch = true;					// if it's true then we can add a renter for the property
				break;
			}
		}
		
		match.add(isOwnIdMatch);
		match.add(isRentIdMatch);
		
		return match;
	}
	
	public boolean insert(Object obj) throws Exception
	{

		Property newRecord = (Property)obj;
		
		ArrayList<Boolean> match = check(newRecord);
		boolean isOwnIdMatch = match.get(0);
		boolean isRentIdMatch = match.get(1);
	
		if(isOwnIdMatch && isRentIdMatch || newRecord.getRent_id() == null) {	// property can be without a renter so renter_id can be null
			String sqlInsert = "INSERT INTO property (own_id,renter_id,zip,town,address,room) VALUES ("
					+newRecord.getOwn_id()+","+newRecord.getRent_id()+","+newRecord.getZip()+",'"+newRecord.getTown()
					+"','"+newRecord.getAddress()+"',"+newRecord.getRoom()+")";
			System.out.println(sqlInsert);
			initConnection();
			Statement st = conn.createStatement();
			st.execute(sqlInsert);
			return true;						// true --> owner found so we can insert (property without owner should not exist)	
												// and no or only one renter is allowed to be
		}
		
		return false;
	}
	
	public boolean update(Object obj, Integer id) throws Exception{
		Property updateRecord = (Property)obj;
		
		ArrayList<Boolean> match = check(updateRecord);
		boolean isOwnIdMatch = match.get(0);
		boolean isRentIdMatch = match.get(1);
		
		if(isOwnIdMatch && isRentIdMatch || updateRecord.getRent_id() == null) {
			String sql = "UPDATE property SET own_id = "+updateRecord.getOwn_id()+", renter_id = "+updateRecord.getRent_id()
					+", zip="+updateRecord.getZip()+", town='"+updateRecord.getTown()+"', address='"+updateRecord.getAddress()
					+"', room="+updateRecord.getRoom()+" WHERE id="+id;
			initConnection();
			Statement st = conn.createStatement();
			st.execute(sql);
			return true;
		}
		return false;
	}
	
	public void delete(Object obj) throws Exception {
		Property newRow = (Property)obj;
		String sql = "DELETE FROM property WHERE id="+newRow.getId();
		
		System.out.println(sql);
		initConnection();
		Statement st = conn.createStatement();
		st.execute(sql);
	}
}
