package hu.pte.dhizri.data;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class RenterDBDAO extends DBConnection implements DataProvider {
	
	public ArrayList<Object> getAll() throws Exception {
		ArrayList<Object> back = new ArrayList<Object>();
		
		initConnection();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT id, name, email, phone FROM renter ORDER BY id ASC");
		//ResultSetMetaData meta = rs.getMetaData();
		while(rs.next()) {
			Integer id = rs.getInt("id");
			String name = rs.getString("name");
			String email = rs.getString("email");
			String phone = rs.getString("phone");
			
			Renter row = new Renter();
			row.setId(id);
			row.setName(name);
			row.setEmail(email);
			row.setPhone(phone);
			
			back.add(row);
		}
		rs.close();
		//conn.close();
		return back;
	}
	
	public boolean insert(Object obj) throws Exception{
		Renter newRecord = (Renter)obj;
		String sql = "INSERT INTO renter (name,email,phone) VALUES ('"+newRecord.getName()+"','"
						+newRecord.getEmail()+"','"+newRecord.getPhone()+"')";
		System.out.println(sql);
		initConnection();
		Statement st = conn.createStatement();
		st.execute(sql);
		
		return true;
	}
	
	public boolean update(Object obj, Integer id) throws Exception{
		Renter updateRecord = (Renter)obj;
		String sql = "UPDATE renter SET name='"+updateRecord.getName()+"', email='"
				+updateRecord.getEmail()+"', phone='"+updateRecord.getPhone()
				+"' WHERE id="+id;
		initConnection();
		Statement st = conn.createStatement();
		st.execute(sql);
		
		return true;
	}
	
	public void delete(Object obj) throws Exception {
		Renter newRow = (Renter)obj;
		String sql = "DELETE FROM renter WHERE id="+newRow.getId();
		
		System.out.println(sql);
		initConnection();
		Statement st = conn.createStatement();
		st.execute(sql);
	}

}
