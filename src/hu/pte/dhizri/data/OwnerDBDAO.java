package hu.pte.dhizri.data;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import hu.pte.dhizri.data.Owner;

public class OwnerDBDAO extends DBConnection implements DataProvider{
	
	public ArrayList<Object> getAll() throws Exception {
		ArrayList<Object> back = new ArrayList<Object>();
		
		DBConnection.initConnection();
		Statement st = DBConnection.conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT id, name, email, phone FROM owner ORDER BY id ASC");
		//ResultSetMetaData meta = rs.getMetaData();
		while(rs.next()) {
			Integer id = rs.getInt("id");
			String name = rs.getString("name");
			String email = rs.getString("email");
			String phone = rs.getString("phone");
			
			Owner row = new Owner();
			row.setId(id);
			row.setName(name);
			row.setEmail(email);
			row.setPhone(phone);
			
			back.add(row);
		}
		rs.close();
		//conn.close();
		return back;
	}
	
	public boolean insert(Object obj) throws Exception{
		Owner newRecord = (Owner)obj;
		String sql = "INSERT INTO owner (name,email,phone) VALUES ('"+newRecord.getName()+"','"
						+newRecord.getEmail()+"','"+newRecord.getPhone()+"')";
		System.out.println(sql);
		DBConnection.initConnection();
		Statement st = DBConnection.conn.createStatement();
		st.execute(sql);
		
		return true;
	}
	
	public boolean update(Object obj, Integer id) throws Exception{
		Owner updateRecord = (Owner)obj;
		String sql = "UPDATE owner SET name='"+updateRecord.getName()+"', email='"
				+updateRecord.getEmail()+"', phone='"+updateRecord.getPhone()
				+"' WHERE id="+id;
		DBConnection.initConnection();
		Statement st = DBConnection.conn.createStatement();
		st.execute(sql);
		return true;
	}
	
	public void delete(Object obj) throws Exception {
		Owner newRow = (Owner)obj;
		String sql = "DELETE FROM owner WHERE id="+newRow.getId();
		
		System.out.println(sql);
		DBConnection.initConnection();
		Statement st = DBConnection.conn.createStatement();
		st.execute(sql);
	}

}
