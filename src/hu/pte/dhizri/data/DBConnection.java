package hu.pte.dhizri.data;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	protected static Connection conn = null;
	//postgres database/ default port: 5432/ database name
	protected static String connectionURL = "jdbc:postgresql://localhost:5432/underlease";
	
	protected static void initConnection() throws Exception {
		if(conn == null) {
			// load jdbc driver
			Class.forName("org.postgresql.Driver");
		
			// connect to database by url, username, password
			conn = DriverManager.getConnection(connectionURL,"postgres","jedi98");
		}
	}
}
