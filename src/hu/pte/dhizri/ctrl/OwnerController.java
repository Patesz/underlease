package hu.pte.dhizri.ctrl;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import hu.pte.dhizri.Main;
import hu.pte.dhizri.Util;
import hu.pte.dhizri.data.DataProvider;
import hu.pte.dhizri.data.Owner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableSelectionModel;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class OwnerController implements Initializable { 
	
	@FXML
		private Button btnInsertOrUpdate;

		@FXML
		private Button btnDelete;
		
		@FXML
		private Button btnClearInput;
			
		@FXML
		private TableView <Owner> tableOwner;
		@FXML
		private TableColumn <Owner, String> colId;
		@FXML
		private TableColumn <Owner, String> colName;
		@FXML
		private TableColumn <Owner, String> colEmail;
		@FXML
		private TableColumn <Owner, String> colPhone;
			
		@FXML
		private TextField txtName;
		@FXML
		private TextField txtEmail;
		@FXML
		private TextField txtPhone;
		
		private boolean isNew = true;
		private Integer id;
		
		//autómatikusan meghívodik amikor meghivodik a kontrollerhez tartozo UI
		public void initialize(URL url, ResourceBundle res) {
			initGrid();
		}
			
		//beírja a cellákba a kapott értékeket amit a getall() function-től kap 
		private void initGrid() {
			DataProvider dao = Main.getOwnerDBDAO();				//lekérem az aktuális példányt
			
			try {
				setButtonState(true,true,false,"Új");
				ArrayList data = dao.getAll();
				
				ObservableList<Owner> oData = FXCollections.observableArrayList(data);
					
				tableOwner.setItems(oData);
							
				colId.setCellValueFactory(Util.createOwnerCol("id"));
				colName.setCellValueFactory(Util.createOwnerCol("name"));
				colEmail.setCellValueFactory(Util.createOwnerCol("email"));
				colPhone.setCellValueFactory(Util.createOwnerCol("phone"));
					
			} catch (Exception e) {
				Util.errorFunc("Nem sikerült lekérni az értékeket a táblázathoz!");
				e.printStackTrace();
			}
		}
			
		public void rowClicked(MouseEvent evt) {
			//kivesszük az alapmodellt
			TableSelectionModel<Owner> model = tableOwner.getSelectionModel();
			
			//elkérjük az adott sort
			Owner own = (Owner) model.getSelectedItem();
			
			id = own.getId();
			txtName.setText(own.getName());
			txtEmail.setText(own.getEmail());
			txtPhone.setText(own.getPhone());
			
			setButtonState(false, false, true, "Módosít");
		}
		
		public void rowNotClicked(MouseEvent evt){
			setButtonState(true, true, false,  "Új");
		}
		
		public void doClean() {
			setButtonState(true, true, false, "Új");
		}
		
		public void setButtonState(boolean clear, boolean isNew, boolean delVisible, String btnInsertUpdateText ) {
			if(clear) {
				clearTxtField();
			}
			this.isNew = isNew;
			btnDelete.setVisible(delVisible);
			btnInsertOrUpdate.setText(btnInsertUpdateText);
			btnClearInput.setVisible(!clear);
		}

		public void doNewOrUpdate(ActionEvent evt) {
			if(Util.isEmpty(txtName.getText())) {
				Util.errorFunc("A név beviteli mező nem lehet üres!");
				return;
			}
			
			if(!Util.isNumber(txtPhone.getText()) || !Util.isPhoneLength(txtPhone.getText(), 11)) {
				Util.errorFunc("A telefonszám beviteli mező csak számot tartalmazhat, illetve 11 karakter hosszú kell legyen.");
				setButtonState(false,true,true,"Új");
				return;
			}
				
			Owner own = new Owner();
			own.setName(txtName.getText());
			own.setEmail(txtEmail.getText());
			own.setPhone(txtPhone.getText());
			
			if(isNew) {
				try {
					Main.getOwnerDBDAO().insert(own);
				} catch (Exception e) {
					Util.errorFunc("Az adott tulajdonos hozzáadása sikertelen!");
					e.printStackTrace();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
			else {
				try {
					Main.getOwnerDBDAO().update(own, id);
				} catch (Exception e) {
					Util.errorFunc("Az adott tulajdonos módósítása sikertelen!");
					e.printStackTrace();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
			initGrid();
		}
			
		public void doDelete(ActionEvent evt) {
			TableSelectionModel<Owner> model = tableOwner.getSelectionModel();
			Owner own = model.getSelectedItem();
			
			if(own != null) {
				try {
					boolean del = Util.confirmFunc("Biztosan törölni szeretné a kijelölt rekordot?");
					if(del) {
						Main.getOwnerDBDAO().delete(own);
						initGrid();
						Util.info("A törlés sikeres.");
					}
					else {
						Util.info("Nem hajtottuk végre a törlést.");
					}
					clearTxtField();
				} catch (Exception e) {
					Util.errorFunc("A törlés sikertelen!");
					e.printStackTrace();
				}
			}
			else {
				Util.errorFunc("Válasszon ki egy sort, ahhoz hogy töröljön!");
			}
		}
		
		public void clearTxtField() {
			txtName.clear();
			txtEmail.clear();
			txtPhone.clear();
		}
	}

