package hu.pte.dhizri.ctrl;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import hu.pte.dhizri.Main;
import hu.pte.dhizri.Util;
import hu.pte.dhizri.data.DataProvider;
import hu.pte.dhizri.data.Property;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableSelectionModel;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class PropertyController implements Initializable{
	
	@FXML
	private Button btnInsertOrUpdate;

	@FXML
	private Button btnDelete;
	
	@FXML
	private Button btnClearInput;

	@FXML
	private TableView <Property> tableProperty;
	@FXML
	private TableColumn <Property, String> colId;
	@FXML
	private TableColumn <Property, String> colOwnId;
	@FXML
	private TableColumn <Property, String> colRentId;
	@FXML
	private TableColumn <Property, String> colZip;
	@FXML
	private TableColumn <Property, String> colTown;
	@FXML
	private TableColumn <Property, String> colAddress;
	@FXML
	private TableColumn <Property, String> colRoom;
	
	@FXML
	private TextField txtOwnId;
	@FXML
	private TextField txtRentId;
	@FXML
	private TextField txtZip;
	@FXML
	private TextField txtTown;
	@FXML
	private TextField txtAddress;
	@FXML
	private TextField txtRoom;
		
	private boolean isNew = true;			// this variable is used, to check if we want to update or insert a record
	private Integer id;
	
	// autómatikusan meghívodik amikor meghivodik a kontrollerhez tartozo UI
	public void initialize(URL url, ResourceBundle res) {
		initGrid();
	}
		
	// beírja a cellákba a kapott értékeket amit a getall() function-től kap 
	private void initGrid() {
		DataProvider dao = Main.getPropertyDBDAO();				// lekérem az aktuális példányt
		
		try {
			setButtonState(true,true,false,"Új");
			ArrayList data = dao.getAll();
			
			ObservableList<Property> oData = FXCollections.observableArrayList(data);
				
			tableProperty.setItems(oData);
			
			colId.setCellValueFactory(Util.createPropertyCol("id"));
			colOwnId.setCellValueFactory(Util.createPropertyCol("own_id"));
			colRentId.setCellValueFactory(Util.createPropertyCol("rent_id"));
			colZip.setCellValueFactory(Util.createPropertyCol("zip"));
			colTown.setCellValueFactory(Util.createPropertyCol("town"));
			colAddress.setCellValueFactory(Util.createPropertyCol("address"));
			colRoom.setCellValueFactory(Util.createPropertyCol("room"));
				
		} catch (Exception e) {
			Util.errorFunc("Nem sikerült lekérni az értékeket a táblázathoz!");
			e.printStackTrace();
		}
	}
		
	public void rowClicked(MouseEvent evt) {
		// kivesszük az alapmodellt
		TableSelectionModel<Property> model = tableProperty.getSelectionModel();
		
		// elkérjük az adott sort
		Property pro = (Property) model.getSelectedItem();
		
		id = pro.getId();
		txtOwnId.setText(Util.safeToString(pro.getOwn_id()));
		if(pro.getRent_id() != null ) {
			txtRentId.setText(Util.safeToString(pro.getRent_id()));
		}
		txtZip.setText(Util.safeToString(pro.getZip()));
		txtTown.setText(pro.getTown());
		txtAddress.setText(pro.getAddress());
		txtRoom.setText(Util.safeToString(pro.getRoom()));
		
		setButtonState(false, false, true, "Módosít");
	}
	
	public void rowNotClicked(MouseEvent evt){
		setButtonState(true, true, false, "Új");
	}
	
	public void doClean(ActionEvent evt) {
		setButtonState(true, true, false, "Új");
	}
	
	public void setButtonState(boolean clear, boolean isNew, boolean delVisible, String btnText ) {
		if(clear) {
			clearTxtField();
		}
		this.isNew = isNew;
		btnDelete.setVisible(delVisible);
		btnInsertOrUpdate.setText(btnText);
		btnClearInput.setVisible(!clear);
	}

	public void doNewOrUpdate(ActionEvent evt) {
		if(Util.isEmpty(txtZip.getText()) || Util.isEmpty(txtTown.getText()) || Util.isEmpty(txtAddress.getText())) {
			Util.errorFunc("Az irányítószám, a város és a cím beviteli mező nem lehet üres!");
			return;
		}
		if(!Util.isNumber(txtZip.getText())) {
			Util.errorFunc("Az irányítószám csak számokat tartalmazhat.");
			setButtonState(false,true,true,"Új");
			return;
		}
		if(!Util.isNumber(txtRoom.getText())) {
			Util.errorFunc("A szobák száma beviteli mező csak számot tartalmazhat.");
			setButtonState(false,true,true,"Új");
			return;
		}
			
		Property pro = new Property();									// creating user defined property type object
		pro.setOwn_id(Integer.parseInt(txtOwnId.getText()));
		if(txtRentId.getText() != null && !txtRentId.getText().trim().isEmpty())	// renter id can be null/empty, so set rent id only if we want to add a renter to this property
			pro.setRent_id(Integer.parseInt(txtRentId.getText()));
		pro.setZip(Short.parseShort(txtZip.getText()));
		pro.setTown(txtTown.getText());
		pro.setAddress(txtAddress.getText());
		pro.setRoom(Short.parseShort(txtRoom.getText()));
		
		if(isNew) {
			try {
				boolean result = Main.getPropertyDBDAO().insert(pro);
				if(!result) {
					Util.warningFunc("Nem találtunk meg a tulajdonost és/vagy a berlő id-jét.");
				}
				
			} catch (Exception e) {
				Util.errorFunc("Az adott ingatlan hozzáadása sikertelen!");
				e.printStackTrace();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				boolean result = Main.getPropertyDBDAO().update(pro, id);
				
				if(!result) {
					Util.warningFunc("Nem találtunk meg a tulajdonost és/vagy a berlő id-jét.");
				}
			} catch (Exception e) {
				Util.errorFunc("Az adott ingatlan módósítása sikertelen!");
				e.printStackTrace();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		initGrid();
	}
		
	public void doDelete(ActionEvent evt) {
		TableSelectionModel<Property> model = tableProperty.getSelectionModel();
		Property pro = model.getSelectedItem();
		
		if(pro != null) {
			try {
				boolean del = Util.confirmFunc("Biztosan törölni szeretné a kijelölt rekordot?");
				if(del) {
					Main.getPropertyDBDAO().delete(pro);
					initGrid();
					Util.info("A törlés sikeres.");
				}
				else {
					Util.info("Nem hajtottuk végre a törlést.");
				}
				clearTxtField();
			} catch (Exception e) {
				Util.errorFunc("A törlés sikertelen!");
				e.printStackTrace();
			}
		}
		else {
			Util.errorFunc("Válasszon ki egy sort, ahhoz hogy töröljön!");
		}
	}
	
	public void clearTxtField() {
		txtOwnId.clear();
		txtRentId.clear();
		txtZip.clear();
		txtTown.clear();
		txtAddress.clear();
		txtRoom.clear();
	}
}
