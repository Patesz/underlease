package hu.pte.dhizri.ctrl;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import hu.pte.dhizri.Main;
import hu.pte.dhizri.Util;
import hu.pte.dhizri.data.DataProvider;
import hu.pte.dhizri.data.Renter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableSelectionModel;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class RenterController implements Initializable{
	
	@FXML
		private Button btnInsertOrUpdate;

		@FXML
		private Button btnDelete;
		
		@FXML
		private Button btnClearInput;
			
		@FXML
		private TableView <Renter> tableRenter;
		@FXML
		private TableColumn <Renter, String> colId;
		@FXML
		private TableColumn <Renter, String> colName;
		@FXML
		private TableColumn <Renter, String> colEmail;
		@FXML
		private TableColumn <Renter, String> colPhone;
			
		@FXML
		private TextField txtName;
		@FXML
		private TextField txtEmail;
		@FXML
		private TextField txtPhone;
			
		private boolean isNew = true;
		private Integer id;
		
		//autómatikusan meghívodik amikor meghivodik a kontrollerhez tartozo UI
		public void initialize(URL url, ResourceBundle res) {
			initGrid();
		}
			
		//beírja a cellákba a kapott értékeket amit a getall() function-tól kap 
		private void initGrid() {
			DataProvider dao = Main.getRenterDBDAO();				//lekérem az aktuális példányt
			
			try {
				ArrayList data = dao.getAll();
				
				ObservableList<Renter> oData = FXCollections.observableArrayList(data);
					
				tableRenter.setItems(oData);
							
				colId.setCellValueFactory(Util.createRenterCol("id"));
				colName.setCellValueFactory(Util.createRenterCol("name"));
				colEmail.setCellValueFactory(Util.createRenterCol("email"));
				colPhone.setCellValueFactory(Util.createRenterCol("phone"));
					
			} catch (Exception e) {
				Util.errorFunc("Nem sikerült lekérni az értékeket a táblázathoz!");
				e.printStackTrace();
			}
		}
			
		public void rowClicked(MouseEvent evt) {
			//kivesszük az alapmodellt
			TableSelectionModel<Renter> model = tableRenter.getSelectionModel();
			
			//elkérjük az adott sort
			Renter rent = (Renter) model.getSelectedItem();
			
			id = rent.getId();
			txtName.setText(rent.getName());
			txtEmail.setText(rent.getEmail());
			txtPhone.setText(rent.getPhone());
			
			isNew = false;
			btnInsertOrUpdate.setText("Módosít");
			btnClearInput.setVisible(true);
		}
		
		public void rowNotClicked(MouseEvent evt){
			setButtonState(true,true,false,"Új");
		}
		
		public void doClean() {
			setButtonState(true,true,false,"Új");
		}
		
		public void setButtonState(boolean clear, boolean isNew, boolean delVisible, String btnText ) {
			if(clear) {
				clearTxtField();
			}
			this.isNew = isNew;
			btnDelete.setVisible(delVisible);
			btnInsertOrUpdate.setText(btnText);
			btnClearInput.setVisible(!clear);
		}

		public void doNewOrUpdate(ActionEvent evt) {
			if(Util.isEmpty(txtName.getText())) {
				Util.errorFunc("A név beviteli mező nem lehet üres!");
				return;
			}
			
			if(!Util.isNumber(txtPhone.getText()) || !Util.isPhoneLength(txtPhone.getText(), 11)) {
				Util.errorFunc("A telefonszám beviteli mező csak számot tartalmazhat, illetve 11 karakter hosszú kell legyen.");
				clearTxtField();
				btnInsertOrUpdate.setText("Új");
				isNew = true;
				return;
			}
				
			Renter rent = new Renter();
			rent.setName(txtName.getText()); 
			rent.setEmail(txtEmail.getText());
			rent.setPhone(txtPhone.getText());
			
			if(isNew) {
				try {
					Main.getRenterDBDAO().insert(rent);
				} catch (Exception e) {
					Util.errorFunc("Az adott bérlő hozzáadása sikertelen!");
					e.printStackTrace();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
			else {
				try {
					Main.getRenterDBDAO().update(rent, id);
				} catch (Exception e) {
					Util.errorFunc("Az adott bérlő módósítása sikertelen!");
					e.printStackTrace();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
			initGrid();
		}
			
		public void doDelete(ActionEvent evt) {
			TableSelectionModel<Renter> model = tableRenter.getSelectionModel();
			Renter rent = model.getSelectedItem();
			if(rent != null && isNew) {
				try {
					boolean del = Util.confirmFunc("Biztosan törölni szeretné a kijelölt rekordot?");
					if(del) {
						Main.getRenterDBDAO().delete(rent);
						initGrid();
						Util.info("A törlés sikeres.");
					}
					else {
						Util.info("Nem hajtottuk végre a törlést.");
					}
					clearTxtField();
				} catch (Exception e) {
					Util.errorFunc("A törlés sikertelen!");
					e.printStackTrace();
				}
			}
			else {
				Util.errorFunc("Válasszon ki egy sort, ahhoz hogy töröljön!");
			}
		}
		
		public void clearTxtField() {
			txtName.clear();
			txtEmail.clear();
			txtPhone.clear();
		}
}
