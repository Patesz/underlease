package hu.pte.dhizri;

import java.util.ArrayList;
import java.util.Optional;

import hu.pte.dhizri.data.Owner;
import hu.pte.dhizri.data.Property;
import hu.pte.dhizri.data.Renter;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class Util {
	
	// converts the object to string if it's not null
	public static String safeToString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	public static Callback<CellDataFeatures<Owner, String>, ObservableValue<String>> createOwnerCol(String attrName) {
		return new PropertyValueFactory<Owner, String>(attrName);
	}
	public static Callback<CellDataFeatures<Renter, String>, ObservableValue<String>> createRenterCol(String attrName) {
		return new PropertyValueFactory<Renter, String>(attrName);
	}
	public static Callback<CellDataFeatures<Property, String>, ObservableValue<String>> createPropertyCol(String attrName) {
		return new PropertyValueFactory<Property, String>(attrName);
	}
	
	// UNUSED - it's clears up all the textfields content that it gets 
	public void clearTextField(ArrayList<TextField> tf) {
		for(int i=0; i<tf.size(); i++) {
			tf.get(i).clear();
		}
	}
	
	// checks if a string is null or empty
	public static boolean isEmpty(String str) {
		if (str == null || str.trim().isEmpty())
		{
			return true;
		}
		else {
			return false;
		}		
	}
	
	// checks if a string only have numerics in it
	public static boolean isNumber(String str) {
		for(int i=0; i<str.length(); i++) {
			if (str.charAt(i) < '0' || str.charAt(i) > '9') {
				return false;
			}
		}
		return true;
	}
	
	// checks if the string length is equals with a number or if it's empty 
	public static boolean isPhoneLength(String str, int len) {
		if(str.length() == len || str.trim().isEmpty()) {
			return true;
		}
		return false;
	}
	
	// shows an error type panel
	public static Alert errorFunc(String contentText) {
		Alert error = new Alert(AlertType.ERROR);
		error.setTitle("Hiba történt!");
		error.setContentText(contentText);
		error.showAndWait();
		return error;
	}
	
	// shows a warning type panel
	public static Alert warningFunc(String contentText) {
		Alert warning = new Alert(AlertType.WARNING);
		warning.setTitle("Figyelmeztetés!");
		warning.setContentText(contentText);
		warning.showAndWait();
		return warning;
	}
	
	// shows confirm panel with an Ok and Cancel button
	public static boolean confirmFunc(String contentText) {
		Alert confirm = new Alert(AlertType.CONFIRMATION);
		confirm.setTitle("Mit szeretne?");
		confirm.setContentText(contentText);
		Optional<ButtonType> result = confirm.showAndWait();
		
		if(result.get() != ButtonType.OK) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static void info(String contentText) {
		Alert info = new Alert(AlertType.INFORMATION);
		info.setTitle("Visszajelzés.");
		info.setContentText(contentText);
		info.showAndWait();
	}
}
